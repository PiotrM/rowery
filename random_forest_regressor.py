import collections
import os
import pickle
from pprint import pprint

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import grid_search
from sklearn.cross_validation import KFold
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import make_scorer

from preprocessing_tools import one_hot, basic_encode

PATH = '/home/piotr/repo/bikes'

train_path = os.path.join(PATH, 'train.csv')
test_path = os.path.join(PATH, 'test.csv')
best_model_path = os.path.join(PATH, 'best_model.pickle')
result_path = os.path.join(PATH, 'result.csv')

# I know, global var; just helps tracking progress of cross validation
# to overcome sklarn limitations
n_scorrings = 0

def rmsle(gold, pred):
    global n_scorrings
    print("scorring:", n_scorrings)
    n_scorrings += 1
    assert gold.shape == pred.shape
    to_sum = (np.log(pred + 1) - np.log(gold + 1)) ** 2.0
    return (np.sum(to_sum) * (1.0 / gold.shape[0])) ** 0.5


class MyClf(object):
    """
     Distinct classifiers for registered and casual rentals.
     This class has only subset of interface of the sklearn clasifier;
    """

    def __init__(self, n_trees, params=None):
        self.n_trees = n_trees

        # self.clfc =  ExtraTreesRegressor(n_trees, n_jobs=7)
        # self.clfr =  ExtraTreesRegressor(n_trees, n_jobs=7)

        # self.clfc = GaussianProcessRegressor(alpha=0.01, n_restarts_optimizer=10, normalize_y=True)
        # self.clfr = GaussianProcessRegressor(alpha=0.01, n_restarts_optimizer=10, normalize_y=True)

        self.clfc = RandomForestRegressor(n_trees, n_jobs=7)
        self.clfr = RandomForestRegressor(n_trees, n_jobs=7)

        if params:
            self.clfc.set_params(**params)
            self.clfr.set_params(**params)

        # self.normalizer = StandardScaler()
        self.normalizer = None

    def fit(self, X, cas, reg):
        if self.normalizer:
            self.normalizer.fit_transform(X)

        # idea of "log" taken from online articles
        self.clfc.fit(X, np.log(cas + 1))
        self.clfr.fit(X, np.log(reg + 1))

    def predict(self, X):
        if self.normalizer:
            self.normalizer.transform(X)

        cas = np.exp(self.clfc.predict(X)).astype(int) - 1
        reg = np.exp(self.clfr.predict(X)).astype(int) - 1

        # linear regression sometimes yield y < 0
        cas[cas < 0] = 0
        reg[reg < 0] = 0

        return cas + reg

    def train_test(self, l1, l2):
        Xt, cast, regt, countt = list_to_features_labels(l1)
        Xv, casv, regv, countv = list_to_features_labels(l2)

        print("training...", end='', flush=True)
        self.fit(Xt, cast, regt)
        print("done")

        err_train = rmsle(self.predict(Xt), countt)
        print("train rmsle", err_train)
        err_valid = rmsle(self.predict(Xv), countv)
        print("valid rmsle", err_valid)
        return err_train, err_valid

    def set_params(self, **params):
        self.clfc.set_params(**params)
        self.clfr.set_params(**params)


def list_to_features_labels(ll):
    """
    convert list of pandas tables representing days to numpy tables
    representing features and labels

    :param ll: list of pandas dataframes
    """
    l = pd.concat(ll)  # type: pd.DataFrame

    if 'count' in l:
        count = l['count']
        casual = l['casual']
        registered = l['registered']

        l.drop('count', 1, inplace=True)
        l.drop('casual', 1, inplace=True)
        l.drop('registered', 1, inplace=True)

        return l.values, casual.values, registered.values, count.values
    else:
        return l.values, None, None, None


def iter_folds(data: pd.DataFrame, n: int):
    q = list(data)
    for tr, te in KFold(len(q), n):
        print("making fold...", end='', flush=True)
        l1 = []
        l2 = []

        for i in tr:
            l1.append(q[i])
        for i in te:
            l2.append(q[i])

        print("done!", flush=True)
        yield l1, l2


def more_features(data: pd.DataFrame):
    """
    Add some not trivial features, convert some variables, drop some features that seems useless
    :param data: input data
    """
    data['freeday'] = data['holiday'] | data['dow'].isin([5, 6])

    if 'casual' in data:
        data['casual'] = data['casual'].astype(float)
        data['registered'] = data['registered'].astype(float)
        data['count'] = data['count'].astype(float)

    # only two years
    data = one_hot(data, 'year')

    # good temperature hunks
    # data['temp_ok'] = data.atemp > 20
    # data['wind_ok'] = data.windspeed < 25
    # data['weather_ok'] = data.weather <= 3

    data = one_hot(data, 'season')
    data = one_hot(data, 'weather')

    # good hours hunks
    # data['to_work'] = (data.hour >= 7) & (data.hour <= 8)
    # data['midday'] = (data.hour >= 11) & (data.hour <= 18)
    # data['from_work'] = (data.hour >= 17) & (data.hour <= 18)

    data.drop('month', axis=1, inplace=True)

    return data


def drop_incompatibile(data: pd.DataFrame):
    """
    return copy of given DataFrame without features that can't by used by sklearn classifiers.
    :param data:
    """
    to_drop = [l for l in data if data[l].dtype == object]
    ret = data.drop(to_drop, 1)
    return ret


def plot_hour(data: pd.DataFrame, hour: int, freeday: bool, style: str, label: str):
    """
    Plot given hour across days
    :param data: data
    :param hour: hour eg. 12, 16
    :param freeday: 0 working day 1: free day
    :param style: pyplot style. eg. 'r-' 'b.'
    """
    if freeday is not None:
        s = data[(data.freeday == freeday) & (data.hour == hour)]
    else:
        s = data[data.hour == hour]

    plt.plot(s.index, s['count'], style, label=label)


def get_best_params(n_trees: int):
    """
    return dictionary of best parameters hopefuly recorderd by after cross validation
    :param n_trees: set n_estimators to this value
    """
    with open(best_model_path, 'br') as f:
        best_clf = pickle.load(f)
        best_params = best_clf.get_params()['estimator'].get_params()
        del best_params['min_impurity_split']
        best_params['n_estimators'] = n_trees

        return best_params

#
# Enter points
#



def classify_test_set(n_trees, use_best_params: bool, use_params: dict = None):
    if use_best_params:
        best_params = get_best_params(n_trees)
        print("using 'best' parameters from CV:")
        pprint(best_params)
    else:
        best_params = use_params

    train = pd.read_csv(train_path)
    train_t = basic_encode(train)
    train_t = more_features(train_t)
    train_t = drop_incompatibile(train_t)

    test = pd.read_csv(test_path)
    test_full = basic_encode(test)
    test_full = more_features(test_full)
    test_t = drop_incompatibile(test_full)

    clf = MyClf(n_trees, best_params)

    Xt, cast, regt, countt = list_to_features_labels([train_t])
    clf.fit(Xt, cast, regt)

    Xv, casv, regv, countv = list_to_features_labels([test_t])
    pred = clf.predict(Xv)
    test_full['count'] = pred

    print("saving")
    r = test_full[['datetime', 'count']]
    r.to_csv(result_path, index=False)
    train_t.reindex(pd.date_range(start=train_t.index[0], end=test_t.index[-1], req='h'))

    plot_hour(train_t, 14, 0, 'r.', "gold")
    plot_hour(test_full, 14, 0, 'b.', "predicted")
    plt.legend()

    plt.show()


def run_cv(n_trees: int, use_best_params: bool, use_params: dict = None):
    if use_best_params:
        best_params = get_best_params(n_trees)
        print("using 'best' parameters from CV:")
        pprint(best_params)
    else:
        best_params = use_params

    train = pd.read_csv(train_path)

    data = basic_encode(train)
    data = more_features(data)

    clf = MyClf(n_trees, best_params)

    g = [drop_incompatibile(x[1]) for x in data.groupby('date')]
    results = ([clf.train_test(tr, va) for tr, va in iter_folds(g, 10)])

    results = np.array(results)
    print("trees: ", n_trees)
    print("results on training set and test set")
    print(results)
    print(results.mean(axis=0), results.std(axis=0))
    print("done!")


def find_best_params(n_trees):
    data = pd.read_csv(train_path)
    data = basic_encode(data)
    data = more_features(data)
    data = drop_incompatibile(data)

    params = collections.OrderedDict({
        'max_depth': (None, 10, 100),
        'min_samples_split': (2, 4, 8, 24),
        'min_samples_leaf': (1, 2, 4, 16, 64),
        'max_leaf_nodes': (None, 2, 128),
        'oob_score': (True, False),
    })

    Xt, cast, regt, countt = list_to_features_labels([data])

    clf = grid_search.GridSearchCV(
        RandomForestRegressor(n_trees, n_jobs=7),
        params, scoring=make_scorer(rmsle, greater_is_better=False),
        cv=3, refit=False, error_score=10000.
    )

    clf.fit(Xt, regt)

    with open(best_model_path, 'bw+') as f:
        pickle.dump(clf, f)

# find_best_params(500)
# classify_test_set(7000, False)
# run_cv(7000, True)
