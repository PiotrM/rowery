from numpy.random.mtrand import RandomState
import pandas as pd


def basic_encode(data):
    #data['datetime'] = pd.to_datetime(data['datetime'])
    data = data.set_index(pd.to_datetime(data['datetime']))

    data['date'] = data.index.date
    data['year'] = data.index.year
    data['month'] = data.index.month
    data['dow'] = data.index.dayofweek
    data['hour'] = data.index.hour
    return data


def do_categorical(data):
    data['dow'] = data['dow'].astype('category')
    data['weather'] = data['weather'].astype('category')
    data['season'] = data['season'].astype('category')
    data['holiday'] = data['holiday'].astype('category')
    data['workingday'] = data['workingday'].astype('category')
    data['holiday'] = data['holiday'].astype('category')


def one_hot(l: pd.DataFrame, column) -> pd.DataFrame:
    dows = pd.get_dummies(l[column], prefix=column+"_")
    l = pd.concat([l, dows], axis=1)
    l.drop(column, axis=1, inplace=True)
    return l
